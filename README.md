adRadar
=======

## Versioning
#### Versioning Used: [Semantic](http://semver.org/)
#### String, lowercase

  - MAJOR ("major") version when you make incompatible API changes
  - MINOR ("minor") version when you add functionality in a backwards-compatible manner
  - PATCH ("patch") version when you make backwards-compatible bug fixes.
  - PRERELEASE ("prerelease") a pre-release version

#### Version example

    major: 1.0.0
    minor: 0.1.0
    patch: 0.0.2
    prerelease: 0.0.1-2
    
## References

* https://www.facebook.com/business/ads-guide/
* https://www.facebook.com/ads/creativehub (View formats/)
* https://www.facebook.com/ads/manager/