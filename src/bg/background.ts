import {Server} from "../class/Server";
import {ISetting} from "../interface/ISetting"

let server: Server = new Server();

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    switch (request.action) {
        case 'sendData': {
            server.sendContent(request.data);
            break;
        }
        case 'sendClick': {
            server.sendClick(request.data);
            break;
        }
        case 'sendUserInfo': {
            server.sendUserInfo(request.data);
            break;
        }
    }
});

/* develblock:start */
chrome.storage.local.get('debug', (setting: ISetting) => {
    const debug: boolean = setting.debug;
    chrome.contextMenus.create({
        "contexts": ['page_action'],
        "title": "Debug",
        "type": "checkbox",
        "checked": debug,
        "onclick": checkboxOnClick
    });
});

function checkboxOnClick(data, tab) {
    chrome.storage.local.set({
        debug: data.checked
    }, function () {
        chrome.tabs.query({url: 'https://www.facebook.com/*'}, function (tabs) {
            console.log('Facebook', tabs);
            tabs.forEach((tab) => {
                chrome.tabs.sendMessage(tab.id, {action: "refresh"}, function (response) {
                    if (response && response.status) {
                        console.info(`Tab ${tab.id} was refreshed.`)
                    }
                });
            });
        });

    });
}
/* develblock:end */