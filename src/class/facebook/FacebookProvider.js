"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Provider_1 = require("../Provider");
const SidebarDetector_1 = require("./detectors/SidebarDetector");
const FeedDetector_1 = require("./detectors/FeedDetector");
class FacebookProvider extends Provider_1.Provider {
    constructor(element, debug) {
        super(element, debug);
        this.detectors = [
            FeedDetector_1.FeedDetector,
            SidebarDetector_1.SidebarDetector
        ];
    }
    parse() {
        if (this.detectAd(this.detectors)) {
            return this.detector.parse();
        }
        else {
            throw Error('Ad is not detected');
        }
    }
    getUserId() {
        let el = document.querySelector('a[accesskey="2"]');
        return el.href;
    }
}
exports.FacebookProvider = FacebookProvider;
