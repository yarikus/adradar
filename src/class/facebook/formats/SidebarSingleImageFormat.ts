import {FormatInterface} from "../../../interface/IFormat";
import {FormatHelper} from "./FormatHelper";

export interface ISingleImageFormat {
    id: string;
    placement: string;
    type: string;
    image: string;
    link?: {
        url: string;
        title?: string;
        text?: string;
    }
}

export class SidebarSingleImageFormat implements FormatInterface {

    public type: string;
    private content: ISingleImageFormat;
    private helper: FormatHelper;

    constructor(public element: Element, public prefix: string, public debug: boolean) {
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__single-image';
        this.helper = new FormatHelper(this.element, this.debug)
    }

    static isFormat(element): boolean {
        return element.querySelectorAll('.e_ry6nkqb_6 img').length === 1;
    }

    public parse() {
        try {
            this.content = {
                id: this.getId(),
                placement: this.prefix,
                type: this.type,
                image: this.getImage(),
                link: {
                    title: this.getTitle(),
                    url: this.getURL(),
                    text: this.getText()
                }
            };

            return (<any>Object).assign(this.content);
        } catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }

    private getId(): string {
        const el= <HTMLElement>this.element;
        const dataset: DOMStringMap = el.dataset;
        return dataset.egoFbid;
    }

    private getImage(): string {
        const el = <HTMLImageElement>this.element.querySelector('.e_ry6nkqb_6 img');
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        if (el && el.width > 200) {
            return el && el.src;
        }
    }

    private getTitle() {
        const el = <HTMLElement>this.element.querySelector('div[title]');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.title;
    }

    private getURL() {
        const el = <HTMLLinkElement>this.element.querySelector('a[target="_blank"]');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.href;
    }

    private getText() {
        const el = <HTMLElement>this.element.querySelector('span');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el.textContent;
    }

}