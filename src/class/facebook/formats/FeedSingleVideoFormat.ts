import {FormatInterface} from "../../../interface/IFormat";
import {FormatHelper} from "./FormatHelper";

export interface ISingleVideoFormat {
    id: string;
    placement: string;
    type: string;
    thumbnail: string;
    text?: string;
    identity?: {
        icon: string;
        name: string;
        link: string;
    }
    link?: {
        button: string;
        url: string;
        title?: string;
        text?: string;
    }
}

export class FeedSingleVideoFormat implements FormatInterface {

    public type: string;
    private content: ISingleVideoFormat;
    private helper: FormatHelper;

    constructor(public element: Element, public prefix: string, public debug: boolean) {
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__single-video';
        this.helper = new FormatHelper(this.element, this.debug)
    }

    static isFormat(element): boolean {
        return element.querySelector('video') && element.querySelector('video').width === 476;
    }

    public parse() {
        try {
            this.content = {
                id: this.helper.getId(),
                placement: this.prefix,
                type: this.type,
                thumbnail: this.getThumbnail()
            };
            return (<any>Object).assign(this.content, this.helper.getLinks(), this.helper.getIdentity(), this.getText());
        } catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }

    private getThumbnail(): string {
        const el = <HTMLImageElement>this.element.querySelector('img._3chq');
        if (el && el.width === 476) {
            return el.src;
        }
    }

    private getText(): object {
        const text: string = this.helper.getText();
        if (text) {
            return {
                text: text
            }
        }
    }

}