import {FormatInterface} from "../../../interface/IFormat";
import {FormatHelper} from "./FormatHelper";

export interface ICarousel {
    image: string;
    text: string;
    button: string;
    url: string;
}

export interface ICarouselFormat {
    id: string;
    placement: string;
    type: string;
    link?: {
        url: string;
        title?: string;
        text?: string;
    }
    carousel: ICarousel[]
}

export class SiebarCarouselFormat implements FormatInterface {

    public type: string;
    private content: ICarouselFormat;
    private helper: FormatHelper;

    constructor(public element: Element, public prefix: string, public debug: boolean) {
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__carousel';
        this.helper = new FormatHelper(this.element, this.debug)
    }

    static isFormat(element): boolean {
        return element.querySelector('ul').classList.contains('uiList');
    }

    public parse() {
        try {
            this.content = {
                id: this.getId(),
                type: this.type,
                placement: this.prefix,
                carousel: this.getCarousel(),
                link: {
                    title: this.getTitle(),
                    url: this.getURL(),
                    text: this.getText()
                }
            };

            return (<any>Object).assign(this.content);
        } catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }

    private getId(): string {
        const el= <HTMLElement>this.element;
        const dataset: DOMStringMap = el.dataset;
        return dataset.egoFbid;
    }

    private getTitle() {
        const el = <HTMLElement>this.element.querySelector('div[title]');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.title;
    }

    private getURL() {
        const el = <HTMLLinkElement>this.element.querySelector('a[target="_blank"]');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.href;
    }

    private getText() {
        const el = <HTMLElement>this.element.querySelector('span');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el.textContent;
    }

    private getCarousel(): ICarousel[] {
        return [].map.call(this.element.querySelectorAll('ul.uiList li'), (li) => {
            const image = li.querySelector('img');
            const link = li.querySelector('[target="_blank"]');
            if (this.debug) {
                image && image.classList.add('this-is-field');
            }
            if (image.width > 100) {
                return {
                    image: image.src,
                    url: link.href || null
                }
            }
        });
    }
}