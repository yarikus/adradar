import {FormatInterface} from "../../../interface/IFormat";
import {FormatHelper} from "./FormatHelper";

export interface ICarousel {
    image: string;
    text: string;
    button: string;
    url: string;
}

export interface ICarouselFormat {
    id: string;
    type: string;
    placement: string;
    carousel: ICarousel[]
}

export class FeedCarouselFormat implements FormatInterface {

    public type: string;
    private content: ICarouselFormat;
    private helper: FormatHelper;

    constructor(public element: Element, public prefix: string, public debug: boolean) {
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__carousel';
        this.helper = new FormatHelper(this.element, this.debug)
    }

    static isFormat(element): boolean {
        return element.querySelector('ul').classList.contains('uiList');
    }

    public parse() {
        try {
            this.content = {
                id: this.helper.getId(),
                type: this.type,
                placement: this.prefix,
                carousel: this.getCarousel()
            };

            return (<any>Object).assign(this.content, this.helper.getIdentity(), this.helper.getLinks('._3bt9'));
        } catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }

    private getCarousel(): ICarousel[] {
        return [].map.call(this.element.querySelectorAll('ul.uiList li'), (li) => {
            const image = li.querySelector('img');
            const text = li.querySelector('._1032');
            const button = li.querySelector('[role="button"]');
            if (this.debug) {
                image && image.classList.add('this-is-field');
            }
            if (image.width > 100) {
                return {
                    image: image.src,
                    text: text.textContent || null,
                    button: button.textContent || null,
                    url: button.href || null
                }
            }
        });
    }
}