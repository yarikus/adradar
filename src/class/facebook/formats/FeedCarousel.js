"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FormatHelper_1 = require("./FormatHelper");
class FeedCarouselFormat {
    constructor(element, prefix, debug) {
        this.element = element;
        this.prefix = prefix;
        this.debug = debug;
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__carousel';
        this.helper = new FormatHelper_1.FormatHelper(this.element, this.debug);
    }
    static isFormat(element) {
        return element.querySelector('ul').classList.contains('uiList');
    }
    parse() {
        try {
            this.content = {
                id: this.helper.getId(),
                type: this.type,
                placement: this.prefix,
                carousel: this.getCarousel()
            };
            return Object.assign(this.content, this.helper.getIdentity(), this.helper.getLinks('._3bt9'));
        }
        catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }
    getCarousel() {
        return [].map.call(this.element.querySelectorAll('ul.uiList li'), (li) => {
            const image = li.querySelector('img');
            const text = li.querySelector('._1032');
            const button = li.querySelector('[role="button"]');
            if (this.debug) {
                image && image.classList.add('this-is-field');
            }
            if (image.width === 300) {
                return {
                    image: image.src,
                    text: text.textContent || null,
                    button: button.textContent || null,
                    url: button.href || null
                };
            }
        });
    }
}
exports.FeedCarouselFormat = FeedCarouselFormat;
