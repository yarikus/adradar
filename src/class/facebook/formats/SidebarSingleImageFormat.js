"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FormatHelper_1 = require("./FormatHelper");
class SidebarSingleImageFormat {
    constructor(element, prefix, debug) {
        this.element = element;
        this.prefix = prefix;
        this.debug = debug;
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__single-image';
        this.helper = new FormatHelper_1.FormatHelper(this.element, this.debug);
    }
    static isFormat(element) {
        return element.querySelectorAll('.e_ry6nkqb_6 img').length === 1;
    }
    parse() {
        try {
            this.content = {
                id: this.getId(),
                placement: this.prefix,
                type: this.type,
                image: this.getImage(),
                link: {
                    title: this.getTitle(),
                    url: this.getURL(),
                    text: this.getText()
                }
            };
            return Object.assign(this.content);
        }
        catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }
    getId() {
        const el = this.element;
        const dataset = el.dataset;
        return dataset.egoFbid;
    }
    getImage() {
        const el = this.element.querySelector('.e_ry6nkqb_6 img');
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        if (el && el.width === 284) {
            return el && el.src;
        }
    }
    getTitle() {
        const el = this.element.querySelector('div[title]');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.title;
    }
    getURL() {
        const el = this.element.querySelector('a[target="_blank"]');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.href;
    }
    getText() {
        const el = this.element.querySelector('span');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el.textContent;
    }
}
exports.SidebarSingleImageFormat = SidebarSingleImageFormat;
