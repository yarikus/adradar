"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FormatHelper_1 = require("./FormatHelper");
class FeedSingleVideoFormat {
    constructor(element, prefix, debug) {
        this.element = element;
        this.prefix = prefix;
        this.debug = debug;
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__single-video';
        this.helper = new FormatHelper_1.FormatHelper(this.element, this.debug);
    }
    static isFormat(element) {
        return element.querySelector('video') && element.querySelector('video').width === 476;
    }
    parse() {
        try {
            this.content = {
                id: this.helper.getId(),
                placement: this.prefix,
                type: this.type,
                thumbnail: this.getThumbnail()
            };
            return Object.assign(this.content, this.helper.getLinks(), this.helper.getIdentity(), this.getText());
        }
        catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }
    getThumbnail() {
        const el = this.element.querySelector('img._3chq');
        if (el && el.width === 476) {
            return el.src;
        }
    }
    getText() {
        const text = this.helper.getText();
        if (text) {
            return {
                text: text
            };
        }
    }
}
exports.FeedSingleVideoFormat = FeedSingleVideoFormat;
