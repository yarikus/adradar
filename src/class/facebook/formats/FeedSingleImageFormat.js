"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FormatHelper_1 = require("./FormatHelper");
class FeedSingleImageFormat {
    constructor(element, prefix, debug) {
        this.element = element;
        this.prefix = prefix;
        this.debug = debug;
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__single-image';
        this.helper = new FormatHelper_1.FormatHelper(this.element, this.debug);
    }
    static isFormat(element) {
        if (element.querySelectorAll('.uiScaledImageContainer img').length < 1) {
            return false;
        }
        else {
            const el = element.querySelector('.uiScaledImageContainer img');
            return el && el.width === 476;
        }
    }
    parse() {
        try {
            this.content = {
                id: this.helper.getId(),
                type: this.type,
                placement: this.prefix,
                image: this.getImage()
            };
            return Object.assign(this.content, this.helper.getIdentity(), this.helper.getLinks('._3bt9'));
        }
        catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }
    getImage() {
        if (this.element.querySelectorAll('.uiScaledImageContainer img').length > 1) {
            return;
        }
        else {
            const el = this.element.querySelector('.uiScaledImageContainer img');
            if (this.debug) {
                el && el.classList.add('this-is-field');
            }
            if (el && el.width === 476) {
                return el && el.src;
            }
        }
    }
}
exports.FeedSingleImageFormat = FeedSingleImageFormat;
