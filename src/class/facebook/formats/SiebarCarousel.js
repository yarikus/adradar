"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FormatHelper_1 = require("./FormatHelper");
class SiebarCarouselFormat {
    constructor(element, prefix, debug) {
        this.element = element;
        this.prefix = prefix;
        this.debug = debug;
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__carousel';
        this.helper = new FormatHelper_1.FormatHelper(this.element, this.debug);
    }
    static isFormat(element) {
        return element.querySelector('ul').classList.contains('uiList');
    }
    parse() {
        try {
            this.content = {
                id: this.getId(),
                type: this.type,
                placement: this.prefix,
                carousel: this.getCarousel(),
                link: {
                    title: this.getTitle(),
                    url: this.getURL(),
                    text: this.getText()
                }
            };
            return Object.assign(this.content);
        }
        catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }
    getId() {
        const el = this.element;
        const dataset = el.dataset;
        return dataset.egoFbid;
    }
    getTitle() {
        const el = this.element.querySelector('div[title]');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.title;
    }
    getURL() {
        const el = this.element.querySelector('a[target="_blank"]');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.href;
    }
    getText() {
        const el = this.element.querySelector('span');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el.textContent;
    }
    getCarousel() {
        return [].map.call(this.element.querySelectorAll('ul.uiList li'), (li) => {
            const image = li.querySelector('img');
            const link = li.querySelector('[target="_blank"]');
            if (this.debug) {
                image && image.classList.add('this-is-field');
            }
            if (image.width === 300) {
                return {
                    image: image.src,
                    url: link.href || null
                };
            }
        });
    }
}
exports.SiebarCarouselFormat = SiebarCarouselFormat;
