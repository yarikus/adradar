"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class FormatHelper {
    constructor(element, debug) {
        this.element = element;
        this.debug = debug;
        this.element = element;
        this.debug = debug;
    }
    getId() {
        return this.element.id;
    }
    getTitle() {
        const el = this.element.querySelector('._5s6c');
        if (this.debug && el && el.textContent.length > 0) {
            el && el.classList.add('this-is-field');
        }
        return el && el.innerText;
    }
    getName() {
        const el = this.element.querySelector('img');
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        return el && el.getAttribute('aria-label');
    }
    getIcon() {
        const el = this.element.querySelector('img');
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        return el && el.src;
    }
    getLink() {
        let el = this.element.querySelectorAll('a')[2];
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el.href;
    }
    getIdentity() {
        const identity = {
            identity: {
                icon: this.getIcon(),
                name: this.getName(),
                link: this.getLink()
            }
        };
        return identity;
    }
    getButton() {
        //let el = <HTMLElement>this.element.querySelector('._275_ a[role="button"]');
        let el = this.element.querySelector('._4jy0:not(.PageLikeButton)');
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        return el && el.textContent;
    }
    getButtonLink() {
        let el = this.element.querySelector('._4jy0');
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        return el && el.href;
    }
    getLinkText(selector) {
        const el = this.element.querySelector(selector || '._5q4r');
        if (this.debug && el && el.textContent.length > 0) {
            el.classList.add('this-is-field');
        }
        return el && el.textContent;
    }
    getText() {
        const el = this.element.querySelector('.userContent p');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.textContent;
    }
    getLinks(linkTextSelector) {
        const buttonText = this.getButton();
        if (buttonText && buttonText.length > 0) {
            const titleText = this.getTitle() || null;
            const text = this.getLinkText(linkTextSelector) || null;
            return {
                link: {
                    button: buttonText,
                    url: this.getButtonLink(),
                    title: titleText,
                    text: text
                }
            };
        }
    }
}
exports.FormatHelper = FormatHelper;
