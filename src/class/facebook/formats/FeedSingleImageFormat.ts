import {FormatInterface} from "../../../interface/IFormat";
import {FormatHelper} from "./FormatHelper";

export interface ISingleImageFormat {
    id: string;
    placement: string;
    type: string;
    image: string;
}


export class FeedSingleImageFormat implements FormatInterface {

    public type: string;
    private content: ISingleImageFormat;
    private helper: FormatHelper;

    constructor(public element: Element, public prefix: string, public debug: boolean) {
        this.element = element;
        this.debug = debug;
        this.type = this.prefix + '__single-image';
        this.helper = new FormatHelper(this.element, this.debug)
    }

    static isFormat(element): boolean {
        if (element.querySelectorAll('.uiScaledImageContainer img').length < 1) {
            return false;
        } else {
            const el: HTMLImageElement = element.querySelector('.uiScaledImageContainer img');
            return el && el.width === 476;
        }
    }

    public parse() {
        try {
            this.content = {
                id: this.helper.getId(),
                type: this.type,
                placement: this.prefix,
                image: this.getImage()
            };

            return (<any>Object).assign(this.content, this.helper.getIdentity(), this.helper.getLinks('._3bt9'));
        } catch (error) {
            if (this.debug) {
                console.error('Format has not parsed:', error.message);
                this.element.classList.add('this-is-error');
            }
        }
    }

    private getImage(): string {
        if (this.element.querySelectorAll('.uiScaledImageContainer img').length > 1) {
            return;
        } else {
            const el = <HTMLImageElement>this.element.querySelector('.uiScaledImageContainer img');
            if (this.debug) {
                el && el.classList.add('this-is-field');
            }
            if (el && el.width === 476) {
                return el && el.src;
            }
        }
    }

}