export interface IIdentity {
    identity: {
        icon: string;
        name: string;
        link: string;
    }
}


export class FormatHelper {

    constructor(public element: Element, public debug: boolean) {
        this.element = element;
        this.debug = debug;
    }

    public getId(): string {
        return this.element.id;
    }

    public getTitle(): string {
        const el = <HTMLElement>this.element.querySelector('._5s6c');
        if (this.debug && el && el.textContent.length > 0) {
            el && el.classList.add('this-is-field');
        }
        return el && el.innerText;
    }

    public getName(): string {
        const el: HTMLImageElement = this.element.querySelector('img');
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        return el && el.getAttribute('aria-label');
    }

    public getIcon(): string {
        const el: HTMLImageElement = this.element.querySelector('img');
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        return el && el.src;
    }

    public getLink(): string {
        let el: HTMLAnchorElement = this.element.querySelectorAll('a')[2];
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el.href;
    }

    public getIdentity(): object {
        const identity: IIdentity = {
            identity: {
                icon: this.getIcon(),
                name: this.getName(),
                link: this.getLink()
            }
        };
        return identity;
    }

    public getButton(): string {
        //let el = <HTMLElement>this.element.querySelector('._275_ a[role="button"]');
        let el = <HTMLElement>this.element.querySelector('._4jy0:not(.PageLikeButton)');
        if (el.hasAttribute('aria-haspopup')) {
            return;
        }
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        return el && el.textContent;
    }

    public getButtonLink(): string {
        let el = <HTMLLinkElement>this.element.querySelector('._4jy0:not(.PageLikeButton)');
        if (this.debug) {
            el && el.classList.add('this-is-field');
        }
        return el && el.href;
    }

    public getLinkText(selector?: string): string {
        const el = <HTMLElement>this.element.querySelector(selector || '._5q4r');
        if (this.debug && el && el.textContent.length > 0) {
            el.classList.add('this-is-field');
        }
        return el && el.textContent;
    }

    public getText() {
        const el = <HTMLElement>this.element.querySelector('.userContent p');
        if (this.debug) {
            el.classList.add('this-is-field');
        }
        return el && el.textContent;
    }

    public getLinks(linkTextSelector?: string): object {
        const buttonText: string = this.getButton();
        if (buttonText && buttonText.length > 0) {
            const titleText: string = this.getTitle() || null;
            const text: string = this.getLinkText(linkTextSelector) || null;
            return {
                link: {
                    button: buttonText,
                    url: this.getButtonLink(),
                    title: titleText,
                    text: text
                }
            };
        }
    }

}