import {Provider} from "../Provider";
import {SidebarDetector} from "./detectors/SidebarDetector";
import {FeedDetector} from "./detectors/FeedDetector";
import {IUserInfo} from "../../interface/IUserInfo";

export class FacebookProvider extends Provider {

    constructor(element: Element, debug: boolean) {
        super(element, debug);
        this.detectors = [
            FeedDetector,
            SidebarDetector
        ];
    }

    public parse() {
        if (this.detectAd(this.detectors)) {
            return this.detector.parse();
        } else {
            throw Error('Ad is not detected');
        }
    }

    static getUserId(): string {
        let el = <HTMLLinkElement>document.querySelector('a[accesskey="2"]');
        return el.href;
    }

    static getUserLanguage(): string {
        return document.querySelector('span[lang]').getAttribute('lang');
    }

    static getUserBirthdate(html): string {
        const day = html.match(/<div><span class="_2ieo">(\d{2}.*?)<\/span><\/div>/);
        const year = html.match(/<div><span class="_2ieo">(\d{4}).*?<\/span><\/div>/);
        if (day && year) {
            return day[1] + ' ' + year[1];
        }
    }

    static getUserPhoneNumber(html): string {
        const phoneNumber = html.match(/<span dir="ltr">([^A-Za-z]*)<\/span>/);
        if (phoneNumber !== null) {
            return phoneNumber[1].replace(/\s+/g, '');
        }
    }

    static getUserGender(html): string {
        const gender = Number(html.match(/"GENDER":(\d)/));
        if (gender !== null) {
            if (gender[1] === 1) {
                return 'male';
            } else if (gender[1] === 2) {
                return 'female';
            }
        }
    }

    static getUserInfo(callback) {
        const userId = FacebookProvider.getUserId();
        let userInfo = {} as IUserInfo;
        if (userId) {
            const xhr = new XMLHttpRequest();
            xhr.open('GET', userId + '/about?section=contact-info', true);
            xhr.send(null);
        }
    }


}