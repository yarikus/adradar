"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FeedSingleVideoFormat_1 = require("../formats/FeedSingleVideoFormat");
const FeedSingleImageFormat_1 = require("../formats/FeedSingleImageFormat");
const FeedCarousel_1 = require("../formats/FeedCarousel");
class FeedDetector {
    constructor(element, debug) {
        this.element = element;
        this.debug = debug;
        this.element = element;
        this.type = 'feed';
        this.debug = debug;
        this.formats = [
            FeedSingleImageFormat_1.FeedSingleImageFormat,
            FeedSingleVideoFormat_1.FeedSingleVideoFormat,
            FeedCarousel_1.FeedCarouselFormat
        ];
    }
    detectFormat(formats) {
        return formats.some((format) => {
            if (format.isFormat(this.element) === true) {
                this.format = new format(this.element, this.type, this.debug);
                const dataset = this.element.dataset;
                dataset.ad = this.format.type;
                return true;
            }
            else {
                return false;
            }
        });
    }
    static isAd(element) {
        const node = element;
        if (!node) {
            return;
        }
        if (node.hasAttribute('data-cursor') && !node.classList.contains('hidden_elem')) {
            if (node.innerHTML.indexOf('>Реклама<') > 0 ||
                node.innerHTML.indexOf('ft[is_sponsored]=1') > 0 ||
                node.querySelectorAll('a._m8c').length > 0) {
                return true;
            }
        }
        else {
            return false;
        }
    }
    parse() {
        if (this.detectFormat(this.formats)) {
            // @todo: it is not works
            try {
                return this.format.parse();
            }
            catch (error) {
                console.log('Format has not parsed:', error.message);
            }
        }
        else {
            console.warn('Unknown format', this.element);
            this.element.classList.add('this-is-warn');
            //throw Error('Unknown format');
        }
    }
}
exports.FeedDetector = FeedDetector;
