"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SidebarSingleImageFormat_1 = require("../formats/SidebarSingleImageFormat");
const SiebarCarousel_1 = require("../formats/SiebarCarousel");
class SidebarDetector {
    constructor(element, debug) {
        this.element = element;
        this.debug = debug;
        this.element = element;
        this.type = 'sidebar';
        this.debug = debug;
        this.formats = [
            SidebarSingleImageFormat_1.SidebarSingleImageFormat,
            SiebarCarousel_1.SiebarCarouselFormat
        ];
    }
    detectFormat(formats) {
        return formats.some((format) => {
            if (format.isFormat(this.element) === true) {
                this.format = new format(this.element, this.type, this.debug);
                const dataset = this.element.dataset;
                dataset.ad = this.format.type;
                return true;
            }
            else {
                return false;
            }
        });
    }
    static isAd(element) {
        const node = element;
        if (!node) {
            return;
        }
        if (node.classList.contains('ego_unit')) {
            return node.querySelectorAll('a[data-gt]').length > 0;
        }
        else {
            return false;
        }
    }
    parse() {
        if (this.detectFormat(this.formats)) {
            // @todo: it is not works
            try {
                return this.format.parse();
            }
            catch (error) {
                console.log('Format has not parsed:', error.message);
            }
        }
        else {
            console.warn('Unknown format', this.element);
            this.element.classList.add('this-is-warn');
            //throw Error('Unknown format');
        }
    }
}
exports.SidebarDetector = SidebarDetector;
