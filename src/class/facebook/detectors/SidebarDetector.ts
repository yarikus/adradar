import {DetectorInterface} from "../../../interface/IDetector";
import {FormatInterface} from "../../../interface/IFormat";
import {SidebarSingleImageFormat} from "../formats/SidebarSingleImageFormat";
import {SiebarCarouselFormat} from "../formats/SiebarCarousel";

export class SidebarDetector implements DetectorInterface {

    format: FormatInterface;

    //@todo: any;
    formats: [any];

    type: string;

    constructor(public element: HTMLElement, public debug: boolean) {
        this.element = element;
        this.type = 'sidebar';
        this.debug = debug;
        this.formats = [
            SidebarSingleImageFormat,
            SiebarCarouselFormat
        ];
    }

    private detectFormat(formats): boolean {
        return formats.some((format) => {
            if (format.isFormat(this.element) === true) {
                this.format = new format(this.element, this.type, this.debug);
                const dataset: DOMStringMap = this.element.dataset;
                dataset.ad = this.format.type;
                return true;
            } else {
                return false;
            }
        });
    }

    static isAd(element): boolean {
        const node: Element = element as Element;
        if (!node) {
            return;
        }
        if (node.classList.contains('ego_unit')) {
            return node.querySelectorAll('a[data-gt]').length > 0;
        } else {
            return false;
        }
    }

    public parse() {
        if (this.detectFormat(this.formats)) {
            // @todo: it is not works
            try {
                return this.format.parse();
            } catch (error) {
                console.log('Format has not parsed:', error.message);
            }
        } else {
            console.warn('Unknown format', this.element);
            this.element.classList.add('this-is-warn');
            //throw Error('Unknown format');
        }
    }
}
