import {DetectorInterface} from "../../../interface/IDetector";
import {FormatInterface} from "../../../interface/IFormat";
import {FeedSingleVideoFormat} from "../formats/FeedSingleVideoFormat";
import {FeedSingleImageFormat} from "../formats/FeedSingleImageFormat";
import {FeedCarouselFormat} from "../formats/FeedCarousel";

export class FeedDetector implements DetectorInterface {

    format: FormatInterface;

    //@todo: any;
    formats: [any];

    type: string;

    constructor(public element: HTMLElement, public debug: boolean) {
        this.element = element;
        this.type = 'feed';
        this.debug = debug;
        this.formats = [
            FeedSingleImageFormat,
            FeedSingleVideoFormat,
            FeedCarouselFormat
        ];
    }

    private detectFormat(formats): boolean {
        return formats.some((format) => {
            if (format.isFormat(this.element) === true) {
                this.format = new format(this.element, this.type, this.debug);
                const dataset: DOMStringMap = this.element.dataset;
                dataset.ad = this.format.type;
                return true;
            } else {
                return false;
            }
        });
    }

    static isAd(element): boolean {
        const node: Element = element as Element;
        if (!node){
            return;
        }

        if (node.hasAttribute('data-cursor') && !node.classList.contains('hidden_elem')) {
            if (node.innerHTML.indexOf('>Реклама<') > 0  ||
                node.innerHTML.indexOf('ft[is_sponsored]=1') > 0 ||
                node.querySelectorAll('a._m8c').length > 0) {
                return true;
            }
        } else  {
            return false;
        }
    }

    public parse() {
        if (this.detectFormat(this.formats)) {
            // @todo: it is not works
            try {
                return this.format.parse();
            } catch (error) {
                console.log('Format has not parsed:', error.message);
            }
        } else {
            console.warn('Unknown format', this.element);
            this.element.classList.add('this-is-warn');
            //throw Error('Unknown format');
        }
    }

}