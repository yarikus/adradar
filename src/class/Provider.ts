import {IAd} from "../interface/IAd";
import {DetectorInterface} from "../interface/IDetector";


export abstract class Provider {
    title: string;

    detector: DetectorInterface;

    //@todo: any
    detectors: [any];

    userId: string;

    constructor(public element: Element, public debug: boolean) {
        this.element = element;
        this.debug = debug;
    }

    detectAd(detectors): boolean {
        return detectors.some((detector) => {
            if (detector.isAd(this.element) === true) {
                this.detector = new detector(this.element, this.debug);
                return true;
            } else {
                return false;
            }
        });
    };

    getHTML(): string {
        return this.element.outerHTML;
    }

}