import {IAd} from "../interface/IAd";

export class Server {

    private address: string;
    private xhr: XMLHttpRequest;

    constructor() {
        this.address = 'http://radar.traff.today/api/v0.1';
        /* develblock:start */
        this.address = 'http://adradar.local';
        /* develblock:end */
        this.xhr = new XMLHttpRequest();
    }

    private sendJSON(method: string, location: string, data: object): void {
        this.xhr.open(method, this.address + location);
        this.xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        this.xhr.send(JSON.stringify(data));
    }

    public sendUserInfo(data): void {
        this.sendJSON('POST', '/user-info', data);
    }

    public sendClick(data): void {
        this.sendJSON('POST', '/click', data);
    }

    public sendContent(data: IAd): void {
        const newData = {
            id: data.id,
            user_id: data.userId,
            type: data.type,
            place: data.placement,
            title: data.link && data.link.title,
            text: data.link && data.link.text,
            link: data.link && data.link.url,
            url:  data.link && data.link.url,
            image_url: data.image || data.thumbnail,
            page_title:  data.identity && data.identity.name,
            page_link:  data.identity && data.identity.link,
            // age: null,
            // gender: null,
            // lang: 'ru',
            // country: 'ru',
            text_2: data.text,
            carousel: JSON.stringify(data.carousel) || null
        };

        const XHR = new XMLHttpRequest();
        let urlEncodedData = "";
        let urlEncodedDataPairs = [];
        let name;

        // Turn the data object into an array of URL-encoded key/value pairs.
        for(name in newData) {
            urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(newData[name]));
        }

        // Combine the pairs into a single string and replace all %-encoded spaces to
        // the '+' character; matches the behaviour of browser form submissions.
        urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

        // Define what happens on successful data submission
        XHR.addEventListener('load', function(event) {
            //alert('Yeah! Data sent and response loaded.');
        });

        // Define what happens in case of error
        XHR.addEventListener('error', function(event) {
            //alert('Oups! Something goes wrong.');
        });

        // Set up our request
        XHR.open('POST', 'http://radar.traff.today/api/v0.1/new.php');

        // Add the required HTTP header for form data POST requests
        XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        // Finally, send our data.
        XHR.send(urlEncodedData);

    }

}