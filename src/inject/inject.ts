import {Provider} from "../class/Provider";
import {FacebookProvider} from "../class/facebook/FacebookProvider";
import {IAd} from "../interface/IAd";
import {ISetting} from "../interface/ISetting"
import {IUserInfo} from "../interface/IUserInfo";

// if (localStorage.getItem('userInfo') === null) {
//     FacebookProvider.getUserInfo((result: IUserInfo) => {
//         localStorage.setItem('userInfo', JSON.stringify(result));
//         chrome.runtime.sendMessage({action: "sendUserInfo", data: result});
//     });
// }

function sendData(node) {
    /* develblock:start */
    chrome.storage.local.get('debug', (setting: ISetting) => {
        /* develblock:end */

        let debug: boolean = false;

        /* develblock:start */
        debug = setting.debug;
        /* develblock:end */

        try {
            const provider: FacebookProvider = new FacebookProvider(node, debug);
            const data = provider.parse();
            data.userId = FacebookProvider.getUserId();
            data.html = provider.getHTML();

            node.addEventListener('click', (event) => {
                chrome.runtime.sendMessage({action: "sendClick", data: {
                    id: data.id,
                    userId: data.userId,
                    clicked: true
                }});
            }, true);

            /* develblock:start */
            if (debug === true) {
                 node.classList.add('this-is-ad');
                console.log('parsed >>>>>>', data);
            }
            /* develblock:end */

            chrome.runtime.sendMessage({
                action: "sendData",
                data: data
            }, function (response) {

            });
        } catch (error) {
            if (debug) {
                if (error.message === 'Ad is not detected') {
                    console.info(error.message);
                } else {
                    console.warn(error.message)
                }
            }
        }
    /* develblock:start */
    });
    /* develblock:end */
}

const observer = new MutationObserver((mutations) => {
    mutations.forEach((mutationNode) => {
        if (mutationNode.addedNodes) {
            for (let i = 0; i < mutationNode.addedNodes.length; i++) {
                const node: Element = <Element>mutationNode.addedNodes[i];
                const parentNode: Element = node.parentNode as Element;
                if (parentNode && parentNode.hasAttribute('data-cursor')) {
                    sendData(node.parentNode);
                } else {
                    try {
                        [].forEach.call(node.querySelectorAll(".ego_unit"), (el) => {
                            sendData(el);
                        });
                    } catch (e) {

                    }
                }
            }
        }
    })
});

observer.observe(document.body, {
    childList: true,
    subtree: true
});

[].forEach.call(document.querySelectorAll("div[data-cursor]"), (node) => {
    sendData(node);
});

[].forEach.call(document.querySelectorAll(".ego_unit"), (node) => {
    sendData(node);
});

/* develblock:start */
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.action === 'refresh') {
        sendResponse({ status: true });
        location.reload();
    }
});
/* develblock:end */

// setInterval(function () {
//     [].forEach.call(document.querySelectorAll("div[data-cursor]"), (node) => {
//         sendData(node);
//     });
// }, 10000);