export interface IUserInfo {
    userId: string;
    phoneNumber: string;
    gender: string;
    birthday: string;
    firstName?: string;
    lastName?: string;
    screen: object;
    userAgent: string;
    datetime: string;
    language: string;
}