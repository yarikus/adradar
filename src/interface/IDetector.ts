import {FormatInterface} from "./IFormat";
export interface DetectorInterface {
    format: FormatInterface;
    parse();
}