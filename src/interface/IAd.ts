export interface IAd {
    id: string;
    userId: string;
    placement: string;
    type: string;
    image?: string;
    thumbnail?: string;
    text?: string;
    identity?: {
        icon: string;
        name: string;
        link: string;
    }
    link?: {
        button: string;
        url: string;
        title?: string;
        text?: string;
    },
    carousel?: {
        image: string;
        text?: string;
        button?: string;
        url?: string;
    }
}