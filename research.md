# Плагин по детектированию рекламы на Facebook

## Задача

Разработать плагин для браузеров по сбору данных рекламных объявлений, показываемых пользователям Facebook во время серфинга внутри социальной сети.

## Закономерности
 * Рекламные блоки делятся по целям.
 * Расположение не влияет на набор полей.
 * Контентные блоки могут содержать разные поля в зависимости от цели и формата рекламы.
 * Расположение блоков влияет на платформу.
 * Контентные блоки варьируются в зависимости от формата рекламы.
 * Набор форматов рекламы зависит от цели и расположения.

## Рекламные блоки

Виды рекламных блоков по расположению на странице:
* лента постов (feed)
  * пост с видео
  * пост картинкой
    * [надпись реклама](images/picture-post.jpg)
* боковая панель (sidebar)

## Детектирование



## Сбор данных

## Действия пользователей
* Клики по объявлениям


## Сохранение данных на сервере

Информация на сервер отправляется сразу после детектирования на странице.

Рекламные объявления:

```json
{
  "id": "",
  "userData": {
    "id": "john.doe",
    "userAgent": "",
    "ipAddress": "",
    "display": "",
    "date": "2017-11-22T08:41:00.011Z",
    "language": "ruRU",
    "gender": "male",
    "age": 28
  }
}
```

Пользовательские действия:

```json
{
  "userId": "john.doe",
  "userAction": "click",
  "adId": "123123"
}
```

## Архитектура

* Inject
  * user actions
  * detect ads
* Server Provider
  * send data to server
* Ads Providers (detect algorithms)
  * Facebook
  * VK

*.facebook.com/*

radar.traff.today/api/v0.1/new.php
key=ddd1094b9c9d42ce67d268bbad27992a

topnews_main_stream_408239535924329 (LitestandStream > getSectionID )

LitestandMessages/NewsFeedLoad

NewsFeedDedupeStoryController

c("LitestandStream").getStoriesSelector() >> "._5jmm"

bigPipe.beforePageletArrive("topnews_main_stream_408239535924329")

https://www.facebook.com/ajax/pagelet/generic.php/LitestandTailLoadPagelet?dpr=1&ajaxpipe=1&ajaxpipe_token=AXh56f_ssTPuv5qc&no_script_path=1&data=%7B%22client_stories_count%22%3A2%2C%22cursor%22%3A%22MTUxMTI1NDIxNDoxNTExMjU0MjE0OjIuNTotNzQ1OTAyNjM0Mzc0ODU5MzQ3MjoxNTExMTg5OTM4OjY0OTA3OTUxMjM0ODYyODM3MTA%3D%22%2C%22feed_stream_id%22%3A10103040%2C%22pager_config%22%3A%22%7B%5C%22edge%5C%22%3Anull%2C%5C%22source_id%5C%22%3Anull%2C%5C%22section_id%5C%22%3A%5C%22408239535924329%5C%22%2C%5C%22pause_at%5C%22%3Anull%2C%5C%22stream_id%5C%22%3Anull%2C%5C%22section_type%5C%22%3A1%2C%5C%22sizes%5C%22%3Anull%2C%5C%22most_recent%5C%22%3Afalse%2C%5C%22ranking_model%5C%22%3Anull%2C%5C%22query_context%5C%22%3A[]%7D%22%2C%22scroll_count%22%3A1%2C%22story_id%22%3Anull%7D&__user=100001680738700&__a=1&__dyn=5V4cjLx2ByK5A9UrJxl0AhEK5EKiWGUG8xdLFwxx-9CyoS2N6wAxubwTwIwHzQ4UJu9xK5WAAzoPBKaxeUPwExmt0gKum4UpyFEgUCu5omxCicwJwQxSayrgS2m4o9E9omUmC-Wx29gqx-EuzFES7-5EbHDBxu3Cq3uexCWK6468nBy9FoO784afwByUvy8lUGdKi2O7kEzyUymf-EOUy68szUmzUFk6eaCyo8J1Wt6g&__req=fetchstream_1&__be=1&__pc=PHASED%3ADEFAULT&__rev=3471224&__spin_r=3471224&__spin_b=trunk&__spin_t=1511256006&__adt=1&ajaxpipe_fetch_stream=1



hyperfeed_story_id_5a13f535152796c71700215
