const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: {
        inject: path.join(__dirname, 'src/inject/inject.ts'),
        background: path.join(__dirname, 'src/bg/background.ts')
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },

    watch: false,
    devtool: false,

    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },

    module: {
        rules: [
            {test: /\.tsx?$/, loader: "awesome-typescript-loader"},
            {test: /\.tsx?$/, loader: 'webpack-strip-block'}
        ]
    },

    plugins: [
        new CleanWebpackPlugin(["dist"]),
        new CopyWebpackPlugin([{
            from: 'manifest.json'
        }, {
            from: 'src/css/inject.css',
            to: 'css/'
        }, {
            from: 'src/icons/',
            to: 'icons'
        }, {
            from: 'src/_locales/',
            to: '_locales'
        }]),
        new UglifyJsPlugin({
            uglifyOptions: {
                output: {
                    comments: false

                },
                compress: {
                    drop_console: true
                }
            }
        })
    ]

};
