import * as assert from 'assert';
import * as jsdom from 'jsdom';
import * as fs from 'fs';
const {JSDOM} = jsdom;
import {FacebookProvider} from "../src/class/facebook/FacebookProvider";

const dir = 'test/samples/facebook/';
const files = fs.readdirSync(dir);


let promises: Array<Promise<any>> = [];

files.map((file) => {
    return dir + file;
}).forEach((file) => {
    promises.push(JSDOM.fromFile(file, {}));
});

function generateTest(document, index) {
    it(document.title, () => {
        const provider: FacebookProvider = new FacebookProvider(document.querySelector('body > div'), true);
        const data = provider.parse();
        assert.equal(typeof data, 'object', 'error');
    });
}

describe('Feed', () => {

    before((done) => {
        Promise.all(promises).then((result) => {
            this.samples = result;
            describe('generated', () => {

                this.samples.forEach((sample, index) => {
                    generateTest(sample.window.document, index);
                });

            });
            done();
        }).catch((err) => {
            done(err);
        });
    });

    // Works only if we have that.
    it('hack');

});
