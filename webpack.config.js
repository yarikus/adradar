const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ChromeExtensionReloader = require('webpack-chrome-extension-reloader');

module.exports = {
    entry: {
        inject: path.join(__dirname, 'src/inject/inject.ts'),
        background: path.join(__dirname, 'src/bg/background.ts')
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },

    watch: true,
    devtool: 'source-map',

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            {test: /\.tsx?$/, loader: "awesome-typescript-loader"}
        ]
    },

    plugins: [
        new CleanWebpackPlugin(["dist"]),
        new CopyWebpackPlugin([{
            from: 'manifest.json'
        }, {
            from: 'src/css/inject.css',
            to: 'css/'
        }, {
            from: 'src/icons/',
            to: 'icons'
        }, {
            from: 'src/_locales/',
            to: '_locales'
        }]),
        new ChromeExtensionReloader({
            port: 9090,
            reloadPage: true,
            entries: {
                inject: 'inject',
                background: 'background'
            }
        })
    ]

};
